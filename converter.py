import os
import config
import subprocess
import shutil

if not os.path.exists(config.DEST_PATH) or not os.path.isdir(config.DEST_PATH):
    os.mkdir(config.DEST_PATH)

if not os.access(config.DEST_PATH, os.W_OK) or not os.access(config.SOURCE_PATH, os.R_OK):
    print("Acesso negado")

def listDirectoryRecursive(path):
    my_list = []
    for x in os.listdir(path):
        if os.path.isdir(os.path.join(path, x)):
            temp = listDirectoryRecursive(os.path.join(path, x))
            if len(temp) > 0:
                my_list = my_list + temp
        else:
            extension = x.split(".")[-1]
            if extension == "mkv" or extension == "mp4" or extension == "avi" or extension == "mov" or extension == "srt":
                my_list.append(os.path.join(path, x))

    return my_list

def listConvertedFiles():
    if not os.path.exists(config.CONVERTED_FILE):
        open(config.CONVERTED_FILE, "w")

    with open(config.CONVERTED_FILE, "r") as f:
        content = f.readlines()
    
    content = [x.strip() for x in content]
    return content

movies = listDirectoryRecursive(config.SOURCE_PATH)
converted = listConvertedFiles()

for movie in movies:

    path = os.path.dirname(movie)
    new_path = path.replace(config.SOURCE_PATH, config.DEST_PATH)

    if not os.path.exists(new_path):
        os.mkdir(new_path)

    movie_new = movie.replace(config.SOURCE_PATH, config.DEST_PATH)
    encontrou = False

    for temp in converted:
        if temp == movie:
            encontrou = True
            break

    if not encontrou:

        extension = os.path.splitext(movie)[1]

        if extension == ".srt":
            shutil.copy2(movie, movie_new)
        else:
            command = "ffmpeg -y -i {0} -c:a copy -sn -map 0:0 -map 0:1 -vf scale=720:-1 -vcodec libx264 {1} 2>&1 >> {2}".format(movie, movie_new, config.LOG_FILE)
            p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
            (output, err) = p.communicate()
            print output
            
        with open(config.CONVERTED_FILE, "a") as file:
            file.write('{0}\n'.format(movie))

    #p = subprocess.Popen("ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 " + x, stdout=subprocess.PIPE, shell=True)
    #(output, err) = p.communicate()
    #print output